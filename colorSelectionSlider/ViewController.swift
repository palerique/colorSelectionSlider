//
//  ViewController.swift
//  colorSelectionSlider
//
//  Created by Paulo Henrique Lerbach Rodrigues on 03/02/18.
//  Copyright © 2018 ph. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var redControl: UISlider!
    @IBOutlet weak var greenControl: UISlider!
    @IBOutlet weak var blueControl: UISlider!

    @IBOutlet weak var colorView: UIView!

    @IBAction func changeColorComponent(_ sender: Any) {

        let r: CGFloat = CGFloat(self.redControl.value)
        let g: CGFloat = CGFloat(self.greenControl.value)
        let b: CGFloat = CGFloat(self.blueControl.value)

        colorView.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}
